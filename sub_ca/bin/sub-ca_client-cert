# !/usr/bin/env bash

#
#      *** Sub-CA - Generate or re-new Client-Certificates ***
#
# https://gitlab.com/sysdef/certificate-authority
#
# (c)2019,2020 Julia 'kaffee' HEINE
# (c)2020 Juergen 'sysdef' HEINE <ca-management@sysdef.de>
#
# Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
# https://creativecommons.org/licenses/by-sa/4.0/
#

# usage: sub-ca_client-cert <sub_ca_id> <client_cert_name>

# exit codes:
#
# 0 - everything worked well
# 1 - ...
# 2 - missing ca name or invalid character
# 3 - missing client cert name or invalid character

sub_ca=$1
client_cert_name=$2

# get and set path
cd $(dirname $0)
cd ..
sub_ca_base_path="$PWD"
sub_ca_data_path="$PWD/data"

now=$(date -u +%Y-%m-%dT%H%MZ)

# set sub-CA paths (global)
set -a
sub_ca_priv_path="${sub_ca_base_path}/priv"
sub_ca_data_path="${sub_ca_base_path}/data"
client_cert_base_path="${sub_ca_base_path}/clients/${client_cert_name}"
client_cert_priv_path="${client_cert_base_path}/private"
client_cert_export_path="${sub_ca_base_path}/export"
set +a

# file names (global)
set -a
#sub_ca_cnf="${sub_ca_data_path}/sub_ca.conf"
#sub_ca_csr="${sub_ca_data_path}/sub_ca.csr"
sub_ca_key="${sub_ca_priv_path}/sub_ca.key"
sub_ca_crt="${sub_ca_data_path}/sub_ca.crt"
sub_ca_pwd="${sub_ca_priv_path}/sub_ca.pwd"

#sub_ca_crl="${sub_ca_data_path}/sub_ca.crl"
#sub_ca_chn="${sub_ca_data_path}/sub_ca-chain.pem"
sub_ca_set="${sub_ca_base_path}/settings.conf"
#sub_ca_tpl="${root_ca_base_path}/templates/sub_ca.conf"
#sub_ca_set_tpl="${root_ca_base_path}/templates/sub_ca_settings.conf"
#sub_ca_crt_ser="${sub_ca_data_path}/sub_ca.crt.srl"
#sub_ca_crl_ser="${sub_ca_data_path}/sub_ca.crl.srl"
#sub_ca_crl_db="${sub_ca_data_path}/sub_ca.db"
#readme_tpl="${root_ca_base_path}/templates/README.txt"

client_cert_tpl="${sub_ca_base_path}/templates/client.conf"

client_cert_key="${client_cert_priv_path}/${client_cert_name}.key"
client_cert_csr="${client_cert_base_path}/${client_cert_name}.csr"
client_cert_cnf="${client_cert_base_path}/${client_cert_name}.conf"


set +a

# import functions
. lib/AUTOLOAD

# set exit code on link fails
NOWLNKEXIT=4

# check for valid CA name
validate_sub_ca $sub_ca_id and exit 2;

# check for valid client cert name
validate_client_cert_name $client_cert_name and exit 3;

# if client_cert_name already exist
[[ -d ${client_cert_base_path} ]] and echo "Certificate directory does already exist" and exit 1;

# create directories
mkdir -p ${client_cert_priv_path}

# configure client config file
cat client_cert_tpl | sed "s/common_name/${client_cert_name}/" > /etc/sslconf/config/${client_cert_name}.conf

## generate key if no csr and no key was provided
openssl genrsa \
  -out ${client_cert_key}

# create CSR if not provided
openssl req \
  -new \
  -sha256 \
  -key    ${client_cert_key} \
  -out    ${client_cert_csr} \
  -config ${client_cert_cnf}

echo -e "\n *** generating intermidiate certificate ***\n"
openssl x509 \
  -req \
  -in         ${client_cert_csr} \
  -CA $       {sub_ca_crt} \
  -CAkey      ${sub_ca_key} \
  -set_serial ${serial} \
  -passin     file:${sub_ca_pwd} \
  -extensions client \
  -days 2 \
  -outform PEM \
  -out /home/ca/${client_cert_name}/${client_cert_name}.crt

echo -e "\n *** generating certificate chain ***\n"
