# Certificate Authority

**Managing a Root CA and Sub CAs by simple scripts, GNU/Linux and OpenSSL**


## Why do i need that and what is that all about?

Please refer

* https://en.wikipedia.org/wiki/Certificate_authority
* https://www.thesslstore.com/blog/root-certificates-intermediate/


## Root CA

 Directory **root_ca** contains the ressources to setup a Root-CA and create Sub-CAs (Intermediate Certificates)

* Initialize Root-CA
* Generate or re-new Sub-CA-Certificates*

## Sub CA

 **WORK IN PROGRESS**

 Directory **sub_ca** contains the ressources to manage a Sub-CA, create certificates and client-certificates for e-mail, domains and code-signing

* Manage the Sub-CA
** Generate or re-new Domain-Certificates*
** Generate or re-new Client-Certificates*
** Create a Sub-Sub-CA*
** Update and upload the CRL

* Manage the Sub-Sub-CA
** Generate or re-new Client-Certificates*
** Update and upload the CRL


***) optionally generate Private Key and CSR in case CSR was not provided**

