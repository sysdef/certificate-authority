

Dear Sub-CA Administrator,


This is the data export of the Sub-CA ${sub_ca}
from ${now} .

You may find the different versions in order of
time in the directories named by the dates.


 # Settings

 Settings of your Root CA      - root_ca_settings.conf
 Settings of your Sub-CA       - ${sub_ca}.conf


 # Files used for generating

 Your Certificate Request      - ${sub_ca}.csr
 Your Sub-CA private Key *     - ${sub_ca}.key


 # Your Certificate files

 Your Intermidiate Certificate - ${sub_ca}.crt
 Your Certificate Chain        - ${sub_ca}-chain.pem

 *) in case it wasn't provided by you


If you think something went wrong, please check the
Settings files and do not hesitate to contact us.

Thank You,
Your Root CA Team of ${root_ca_orga_name}.
