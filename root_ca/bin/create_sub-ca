# !/usr/bin/env bash

#
#      *** Create and Update SUB-CAs ***
#
# https://gitlab.com/sysdef/certificate-authority
#
# (c)2019,2020 Julia 'kaffee' HEINE
# (c)2020 Juergen 'sysdef' HEINE <ca-management@sysdef.de>
#
# Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
# https://creativecommons.org/licenses/by-sa/4.0/
#

# exit codes:
#
# 1 - config missing
# 2 - missing ca name or invalid character
# 3 - ca does already exist
# 4 - link to time based file name failed
# 5 - sub ca settings file does not exist
# 6 - failed to set sub-ca password
#

sub_ca=$1

# get and set path
cd $(dirname $0)
cd ..
root_ca_base_path="$PWD"
root_ca_data_path="$PWD/data"

now=$(date -u +%Y-%m-%dT%H%MZ)

# set sub-CA paths (global)
set -a
sub_ca_base_path="${root_ca_base_path}/sub_ca/${sub_ca}"
sub_ca_priv_path="${sub_ca_base_path}/priv"
sub_ca_data_path="${sub_ca_base_path}/data"
sub_ca_export_path="${root_ca_base_path}/export"
set +a

# file names (global)
set -a
sub_ca_cnf="${sub_ca_data_path}/sub_ca.conf"
sub_ca_csr="${sub_ca_data_path}/sub_ca.csr"
sub_ca_key="${sub_ca_priv_path}/sub_ca.key"
sub_ca_pwd="${sub_ca_priv_path}/sub_ca.pwd"
sub_ca_crt="${sub_ca_data_path}/sub_ca.crt"
sub_ca_crl="${sub_ca_data_path}/sub_ca.crl"
sub_ca_chn="${sub_ca_data_path}/sub_ca-chain.pem"
sub_ca_set="${sub_ca_base_path}/settings.conf"
sub_ca_tpl="${root_ca_base_path}/templates/sub_ca.conf"
sub_ca_set_tpl="${root_ca_base_path}/templates/sub_ca_settings.conf"
root_ca_cnf="${root_ca_data_path}/root_ca.conf"
root_ca_crt="${root_ca_data_path}/root_ca.crt"
root_ca_set="${root_ca_base_path}/settings.conf"
sub_ca_crt_ser="${sub_ca_data_path}/sub_ca.crt.srl"
sub_ca_crl_ser="${sub_ca_data_path}/sub_ca.crl.srl"
sub_ca_crl_db="${sub_ca_data_path}/sub_ca.db"
readme_tpl="${root_ca_base_path}/templates/README.txt"
set +a

# import functions
. lib/AUTOLOAD

# set exit code on link fails
NOWLNKEXIT=4

# check for valid CA name
validate_ca_name $sub_ca || exit 2;

# create directories
mkdir -p {${sub_ca_base_path},${sub_ca_priv_path},${sub_ca_data_path},${sub_ca_export_path}}
chmod 700 {${sub_ca_base_path},${sub_ca_priv_path},${sub_ca_data_path},${sub_ca_export_path}}

# get root CA organization Name
root_ca_orga_name=$( grep "organizationName=" "${root_ca_set}" | sed 's/organizationName=//' )

# check for settings
[[ -e "${sub_ca_set}" ]] || {

  cp "${sub_ca_set_tpl}" "${sub_ca_set}"
  chmod +x "${sub_ca_set}"

  # set root ca orga name if a match is required
  grep -q "organizationName_match=match" "${root_ca_set}" && {
    sed -i 's/organizationName=.*/organizationName='"${root_ca_orga_name}"' # MUST MATCH THE ROOT-CA! DO NOT EDIT!/' ${sub_ca_set}
  }

  echo -e "\nPlease configure the Sub-CA settings in ${sub_ca_set} and start this command again."
  exit 1

}

# import settings
set -a
[[ -e "${sub_ca_set}" ]] || {
  echo "ERROR: sub ca settings file '${sub_ca_set}' not found."
  exit 5
}
source ${sub_ca_set}
set +a

# configure sub ca config file
replace='
  $ca_name
  $base_url
  $sub_ca_base_path
  $default_bits
  $default_days
  $countryName
  $stateOrProvinceName
  $organizationName
  $organizationalUnitName
  $commonName
  $sub_ca_data_path
  $sub_ca_crt
  $sub_ca_key
  $sub_ca_crt_ser
  $sub_ca_crl_ser
  $sub_ca_crl_db'

envsubst "$replace" < ${sub_ca_tpl} > ${sub_ca_cnf}

# create link with date
now_lnk "${sub_ca_cnf}"

# generate key if no csr and no key was provided
[[ -e ${sub_ca_key} && -e ${sub_ca_csr} ]] || {

  echo -e "\n *** generating key ***\n"

  echo -e "Please set a password for the private key of the new Sub-CA.\n"

  PWD_MINLENGTH=10
  password new_pwd && {
    echo $new_pwd > ${sub_ca_pwd}
  } || {
    echo "ERROR: failed to set password."
    exit 6
  }

  openssl req -new \
    -config "${sub_ca_cnf}" \
    -passout file:${sub_ca_pwd} \
    -out    "${sub_ca_csr}" \
    -keyout "${sub_ca_key}"
  now_lnk   "${sub_ca_key}"

}

# create certificate request if not provided
[[ -e ${sub_ca_csr} ]] || {

  echo -e "\n *** generating csr ***\n"
  openssl req -new \
    -config "${sub_ca_cnf}" \
    -passin file:${sub_ca_pwd} \
    -out    "${sub_ca_csr}" \
    -keyin  "${sub_ca_key}"
  now_lnk   "${sub_ca_csr}"

}

echo -e "\n *** generating intermidiate certificate ***\n"
# TODO: suppress questions if possible
unlink ${sub_ca_crt} 2>/dev/null
openssl ca \
  -config "${root_ca_cnf}" \
  -in     "${sub_ca_csr}" \
  -out    "${sub_ca_crt}" \
  -extensions signing_ca_ext
now_lnk   "${sub_ca_crt}"

echo -e "\n *** generating certificate chain ***\n"
# TODO: add comments (ca name) to the certs
cat       "${sub_ca_crt}" \
          "${root_ca_crt}" > \
          "${sub_ca_chn}"

echo " * exporting to ${sub_ca_base_path}/export/${sub_ca}/$now"
# export
sub_ca_export_now="${sub_ca_base_path}/export/${sub_ca}/$now"
mkdir -p "${sub_ca_export_now}"
chmod 700 "${sub_ca_export_now}"
cd "${sub_ca_export_now}"

# copy files
cat "${root_ca_cnf}" > root_ca_settings.conf
cat "${sub_ca_csr}"  > "${sub_ca}.csr"
cat "${sub_ca_crt}"  > "${sub_ca}.crt"
cat "${sub_ca_cnf}"  > "${sub_ca}.conf"
[[ -e "${sub_ca_key}" ]] && \
cat "${sub_ca_key}"  > "${sub_ca}.key"
cat "${sub_ca_chn}"  > "${sub_ca}-chain.pem"
cat "${root_ca_crt}" > root_ca.crt


# create readme file
envsubst "${sub_ca} ${now} ${root_ca_orga_name}" < ${readme_tpl} > README.txt

cd ..

tar -cvzf "${sub_ca_export_path}/${sub_ca}.tar.gz" *
now_lnk   "${sub_ca_export_path}/${sub_ca}.tar.gz"

ls -l ${sub_ca_export_path}
